# Mock community file preparation

The mock community was prepared in a two step process

1. Retrieving sequence IDs from the community files with [species_list.py](../Programs/species_list.py)
* generates a file containing IDs of members of the mock community
2. Manual addition of the desired number of occurences of each seqence in the final file.
* values are separated with a *space*
3. Forming the final community file using the frequencies in the ID list and the original mock community file with [community_file.py](../Programs/community_file.py)

### 1. Retrieving IDs from community file

```Shell
python3.5 species_list.py --in community.fasta --out IDfile.csv
```

### 2. Manual addition of the desired occurrences

The community was designed with a couple of abundant species and a tail of rare species. The structure can be found in [this file](/community/IDfile.csv).

### 3. Final community file creation

```Shell
python3.5 community_file.py --list IDfile.csv --community community.fasta --out FinalCommunity.fasta
```
