from ggplot import *
import numpy as np
import pandas as pd
import argparse
from Bio import SeqIO

parser = argparse.ArgumentParser()

parser = argparse.ArgumentParser(description='# This program plots some statistical data extracted from the SINA taxonomic assignment .csv file.')

parser.add_argument('-t','-taxonomy', type=str, dest='IN1', help='Original mock community.')
parser.add_argument('-i','-in', type=str, dest='IN2', help='Taxonomic assignment .csv file from SINA.')
parser.add_argument('-o', '-out', type=str, dest='OUT', help='Output file name.')
parser.add_argument('-ot', '-outtable', type=str, help='Taxonomic verification table file name.')
args = parser.parse_args()

input_handle1 = open(args.IN1, 'r')

# Loading the original taxonomy file to parse and use as a template
headers = []
for record in SeqIO.parse(input_handle1, "fasta") :
    headers.append(record.description)
input_handle1.close()

# Splitting the headers and removing IDs
community = []
for entry in headers:
    entry = entry.split(" ")
    entry = ' '.join(entry[1:])
    community.append(entry)

# Preparing and empty data frame
data = pd.DataFrame(columns=[0,1,2,3,4,5,6])

# Filling the data frame with values from the taxonomy file
for line in community:
    line = line.split(';')
    {0:line[0], 1:line[1], 2:line[2], 3:line[3], 4:line[4], 5:line[5], 6:line[6]}
    temp = pd.DataFrame({0:line[0], 1:line[1], 2:line[2], 3:line[3], 4:line[4], 5:line[5], 6:line[6]},index=[0])
    data = pd.concat([data, temp])

###
# Loading the taxonomy file to verify

input_handle2 = open(args.IN2, 'r')

taxonomy = pd.DataFrame.from_csv(input_handle2, sep = ';')

taxonomy = taxonomy['lca_tax_slv']

checkDF = pd.DataFrame(columns=[0,1,2,3,4,5,6])
for line in taxonomy:
    line = line.split(';')
    level = 0
    eletvector = {}
    for element in line:
        toggle = 'not assigned'
        if any(data[level].isin([element])):
            toggle = 'assigned'
        eletvector.update({level:toggle})
        level += 1
    checkDF = checkDF.append(eletvector, ignore_index = True)

checkDF.fillna('unkown', inplace=True)

checkDF = checkDF[list(range(0,6))]

checkDFtransposed = checkDF.transpose()
checkDFtransposedMelt = pd.melt(checkDFtransposed)


output_handle = open(args.OUT, 'w')

checkDFtransposedMelt.to_csv(output_handle)
output_handle.close()

output_handle2 = open(args.ot, 'w')
checkDF.to_csv(output_handle2)
output_handle2.close()
