# Closely related community summary with insert lenth 0

This is a summary file of the closely related community analysis with the **pirs2.0.0** and **reago1.1**

## Versions of used programs

More details about the versions of programs used in this study can be find in the [following fle](programVersions.md).

## Overview of the run

1. Previously used closely related community of Streptomyces and its original shape  was used in this run. More information can be found [here](community/summary.md). The erroneous species were kept in place since their assignment down to _genera_ is known.
2. _In silico_ [shotgun sequencing simulation](sequencing/summary.md) (with **Pirs2.0.0**)
  * Inset of length 100 was used in this run. That is the lowest possible value allowed.
3. **Reago1.1** [assembly two step run](assembly/summary.md).
  * Identification of bacterial sequences in the input dataset
  * Gene assembly with **Reago1.1**

## Verification of the results

1. [Taxonomic assignment of the generated contigs with SINA.](verification/summary.md)

## Important note on the results

It turned out, that Silva v123 has multiple errors on the level of specie assignment. This will be further addressed. More information and the progress report can be found within the issue #5.
