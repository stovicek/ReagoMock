# Closely related community summary

This is a summary file of the closely related community analysis with the **pirs2.0.0** and **reago1.1**

## Versions of used programs

More details about the versions of programs used in this study can be find in the [following fle](programVersions.md).

## Overview of the run

1. [Generating of the community](communityPreparation.md).
  * The sequences were retrieved from the Silva database v123 release.
  * Setting the [abundances of different species](community/IDfile.csv) (with local python program).
2. _In silico_ [shotgun sequencing simulation](sequencing/summary.md) (with **Pirs2.0.0**)
3. **Reago1.1** [assembly two step run](assembly/summary.md).
  * Identification of bacterial sequences in the input dataset
  * Gene assembly with **Reago1.1**

## Verification of the results

1. [Taxonomic assignment of the generated contigs with SINA.](verification/summary.md)

## Important note on the results

It turned out, that Silva v123 has multiple errors on the level of specie assignment. This will be further addressed. More information and the progress report can be found within the issue #5.

Nevertheless most contigs generated during theis run were correctly assigned to their genera as can be seen in the [results](verification/summary.md). This makes this run successful.
