from Bio import SeqIO
import argparse

parser = argparse.ArgumentParser()

parser = argparse.ArgumentParser(description='# This program is designed to retrieve sequence IDs from FASTA file generated based on the SILVA database.')

parser.add_argument('--in', dest='IN', help='Input database in .fasta format.')
parser.add_argument('--out', dest='OUT', help='Output file name.')

args = parser.parse_args()

record_dict = SeqIO.index(args.IN, "fasta")

myList = []

for record in record_dict:
    myList.append(record)

myList = '\n'.join(myList)

f = open(args.OUT, 'w')
f.write(myList)
f.close()
