# Close community assembly _Streptomyces_

This is a summary of the assembly of the closely related mock community of the _Streptomyces_ genera with **Reago1.1**.
The analysis was run in the [Anaconda generated Python2.7 environment](../python2.7conda.yml)


1. Filtering bacterial 16S genes
* Results can be found in the [filter_out.7z archive](filter_out.7z)

```Shell
(python2.7conda) : python ~/Bioinformatics/reago-1.1/filter_input.py sequencing/Read1.fasta sequencing/Read2.fasta assembly/filter_out ~/Bioinformatics/reago-1.1/cm ba 6
```
_No sequences were discarded during  this run._

2. Assembly of the filtered sequences

```Shell
(python2.7conda) : python ~/Bioinformatics/reago-1.1/reago.py assembly/filter_out/filtered.fasta assembly/sample_out -l 100
```

Results can be found in the [sample_out folder](sample_out). No [full length genes](sample_out/full_genes.fasta) were recovered, but the program generated [multiple contigs](/sample_out/fragments.fasta).

3. Next step is the [taxonomic verification of the result](../verification/summary.md).

4. Since the results were unsatisfactory, additional [chimera check](../uchime/summary.md) was performed with the program UCHIME.
