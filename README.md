# Reago 16S rRNA gene assembly

***

This is a project has two major focuses:

1. Testing the feasibility of the assembly of multiple 16S rRNA genes from a shotgun sequencing dataset
2. Adjusting parameters of the Reago1.1 program in order to get the best possible results based on mock community dataset

## Major milestones

Exploring the performance of the Reago1.1 with the following datasets:

1. [Distantly related species](DistantCommunity/summary.md).
  - [Shortcut to the results of the run.](DistantCommunity/verification/summary.md)
2. [Closely related species of Streptomyces](CloseCommunityStreptomyces/summary.md).
  - [Shortcut to the results of the run.](CloseCommunityStreptomyces/verification/summary.md)
3. [Closely related species of Streptomyces with insert of a 100bp (smallest possible)](CCS_insert_100bp/summary.md).
  - [Shortcut to the results of the run.](CCS_insert_100bp/verification/summary.md)
4. [Natural community mimicking an environmental sample](naturalCommunity/summary.md)
