from ggplot import *
import numpy as np
import pandas as pd
import argparse

parser = argparse.ArgumentParser()

parser = argparse.ArgumentParser(description='# This program plots some statistical data extracted from the SINA taxonomic assignment .csv file.')

parser.add_argument('-i','-in', type=str, dest='IN', help='Taxonomic assignment .csv file from SINA.')
#parser.add_argument('-out', dest='OUT', help='Output file name.')
parser.add_argument('-o','-outFolder', type=str, dest='OUTFOLDER', help='Output folder name.')

parser.add_argument('-l', '-lower', type=int, help='A lower limit of the quality plot.')

parser.add_argument('-u', '-upper', type=int, help='An upper limit of the quality plot.')

args = parser.parse_args()

input_handle = open(args.IN, 'r')
#input_handle = open('../DistantCommunity/verification/arb-silva.de_align_resultlist_304754.csv', 'r')
#output_handle = open(args.OUT, 'w')
#output_handle = open('output.csv', 'w')

#myDataFrame = pd.DataFrame.from_csv(input_handle, sep = ';')

# Reading the input file into a data frame
myDataFrame = pd.read_csv(input_handle, sep = ';')

myDataFrame.to_csv("sequenceDataFrame.csv")

input_handle.close()

# Plotting an alignment template similarity
gg = ggplot(aes(x = 'sequence_number', y= 'identity'), data = myDataFrame)
gg = gg + geom_bar(stat = 'identity') + scale_y_continuous(limits=(args.l, args.u)) + ggtitle('Template similaritiy') + xlab('Sequence') + ylab('Similarity to template [%]')
ggsave(gg, args.OUTFOLDER + 'Similarity_plot.png')

# Preparing a beginning and end position for the contigs
end = myDataFrame['ecolipos'] + myDataFrame['bps']
myDataFrame['end'] = end

middle = (myDataFrame['end'] + myDataFrame['ecolipos'])/2

myDataFrame['middle'] = middle

myDataFrame['middle']

ranges = aes(ymin = myDataFrame['ecolipos'], ymax = myDataFrame['end'])

# Plotting assembled contigs position
gg = ggplot(aes(x = 'sequence_number', y = 'middle'),data = myDataFrame)
gg = gg + geom_linerange(ranges) + xlab('Sequence number') + ylab('Position in the gene') + ggtitle('Assembled contigs')
ggsave(gg, args.OUTFOLDER + '/Assembled_contigs.png')
