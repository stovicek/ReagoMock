# Version file

Following are versions of the programs used in the DistantCommunity experiment.

### Pirs 2.0.0

```Shell
pirs --version
Program:       pirs (Profile-based Illumina pair-end Reads Simulator)
Version:       2.0.0
Author:        Jianying Yuan (BGI-Shenzhen)
Contact:       yuanjianying@genomics.org.cn
Compile Date:  Nov 11 2015 time: 17:24:53

Usage: pirs <command> [option]
    diploid     generate diploid genome.
    simulate    simulate Illumina reads.
```
### Reago 1.1

REAGO v1.1

#### Dependencies

*Infernal 1.1.1*

Infernal - inference of RNA secondary structure alignments
http://infernal.janelia.org/
Version 1.1.1; July 2014
Copyright (C) 2014 Howard Hughes Medical Institute.

*readjoiner 1.2*

```Shell
gt readjoiner --version
Readjoiner: a string graph-based sequence assembler

version 1.2

GenomeTools version:
gt (GenomeTools) 1.5.3 (2014-09-11 11:40:52)
Copyright (c) 2003-2014 G. Gremme, S. Steinbiss, S. Kurtz, and CONTRIBUTORS
Copyright (c) 2003-2014 Center for Bioinformatics, University of Hamburg
See LICENSE file or http://genometools.org/license.html for license details.

Used compiler: cc (Ubuntu 4.9.1-13ubuntu1) 4.9.1
Compile flags: -g -O2 -fstack-protector-strong -Wformat -Werror=format-security -g -Wall -Wunused-parameter -pipe -fPIC -Wpointer-arith -O3
```

### Anaconda environment

The *reago 1.1* was run in the [following environment](python2.7conda.yml).
