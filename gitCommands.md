# Following are git commands that might be handy

```Shell
# Setting a username
git config --global user.name ADD YOUR USERNAME

# Setting an email
git config --global user.email ADD YOUR EMAIL

# Retrieving current settings
git config --global --list

touch file.txt
# This will create a file

# Adding a file  to the git
git add README.md

# Writing a commit
git commit -m 'Improving the README.md file.'

# Checking the status of the server
git checkout

# Updating files on the server
git push -u origin master

# Updating local files
git pull
```
