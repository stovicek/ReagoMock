# Mock community file preparation

The mock community was prepared in a following process from a .fasta file with sequences pulled from the SIVA database (community file). An OTU table with sequences corresponding to a sample C1 from the Ahuva dataset (2015) was used to generate an ID file with abundances and taxonomic codes of each OTU. Those have been used to generate a mock community fasta file.

1. Generating an ID file from an OTU table with the number of occurrences of each 'specie' with [idFileGenerator.py](../../Programs/idFileGenerator.py).
2. Retrieving relevant sequences from a Silva database file with [subsetSilva.sh](../../subsetSilva.sh).
* Generates a small(er) fasta file with sequences mentionned in the ID file.
3. Forming the final community file using the frequencies in the ID list and the generated sequence file with [community_file.py](../../Programs/community_file.py)

### 1. Retrieving IDs from community file

```Shell
python3.5 species_list.py --in Mock_community_DNA.fasta --out IDfile.csv
```

### 2. Manual addition of the desired occurrences

The community was designed with a couple of abundant species and a tail of rare species. The structure can be found in [this file](IDfile.csv), consisting of sequence codes and the number of occurrences separated by a space.

### 3. Final community file creation

```Shell
python3.5 community_file.py --list IDfile.csv --community Mock_community.fasta --out Mock_community_DNA_multiple_copies.fasta
```
