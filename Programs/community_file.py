from Bio import SeqIO
from Bio.Alphabet import RNAAlphabet
import argparse
import os.path
import os

parser = argparse.ArgumentParser()

parser = argparse.ArgumentParser(description='# This program accepts the input from the species_list.py, with numbers of different sequences separated with one white space and following each ID.')

parser.add_argument('--list', dest='list', help='Sequence list with number of occurences separated by a white space.')
parser.add_argument('--community', dest='community', help='Sequence file obtained from SILVA database.')
parser.add_argument('--out', dest='OUT', help='Output file name.')
parser.add_argument('--sep', dest='SEP', default=' ', help='ID file separator. [Default: " "; Blank space]')
parser.add_argument('--edit', dest='EDIT', default='False', help='Taxonomic codes in the headers of the fasta file need to be edited before use. Accepts only T/F values. [Default: F]')
parser.add_argument('--editname', dest='EDITNAME', default='community_edited.fasta', help='Edited community output name. [Default: community_edited.fasta]')

args = parser.parse_args()

if os.path.isfile(args.EDITNAME):   # If the edit file exists already, it will be deleted.
    print("Edited community file already exists. Do you want to remove the old version? [yes/no]")
    yes = set(['yes','y', 'ye', ''])
    no = set(['no','n'])

    choice = input().lower()
    if choice in yes:
        os.remove(args.EDITNAME)
    elif choice in no:
        print("Exitting the program.")
        quit()
    else:
        raise ValueError("Invalid answer. Please use 'yes' or 'no' answer.")

input_handle = open(args.list, 'r')
output_handle = open(args.OUT, 'w')

keyList = []    # List of keys used to determin redundant keys
skipper = "OFF" # Skipper makes sure the program skips not only redundant header, but also the sequence

counter = 0

if args.EDIT == "T":
    with open(args.community) as fComm, open(args.EDITNAME, 'a') as fTrim: # Trimming tax codes in the community file
        for line in fComm:
            if line[0] == ">": # Header file ineeds to be edited
                line = line.split(' ')
                line[0] = line[0].split('.')[0]
                if line[0] in keyList:
                    skipper = "ON"  # Sequence is redundant, skipper is turned on
                    continue
                else:
                    fTrim.write(' '.join(line))
                    keyList.append(line[0])
            else: # Sequence file
                if skipper == "ON": # Skipper had been turned on, this sequence is redundant
                    skipper = "OFF" # Skipper is turned OFF first
                    continue    # Skipping this loop
                fTrim.write(line)   # Writing output to the file.

with open("keyList.txt", 'a') as fTest:
    for item in keyList:
        fTest.write(item + "\n")

if args.EDIT == 'T':
    record_dict = SeqIO.index(args.EDITNAME, "fasta")
    print("Dictionary loaded successfully.")
elif args.EDIT == 'F':
    record_dict = SeqIO.index(args.community, "fasta")
else:
    print("Error: Unknown value of the --edit argument. Only T/F are accepted. ")

#for key in record_dict:
#   print("key: {0} , value: {1}".format(key, record_dict[key]))

#import pdb; pdb.set_trace()

totalLength = 0
notFound = 0

for line in input_handle:
    totalLength += 1
    line = line.rstrip('\n')
    line = line.split(args.SEP)
    iterator = int(line[1])
    if ('>'+line[0]) not in keyList:
        notFound += 1
        continue
    i = 0
    while i < iterator:
        mySeq =  record_dict[line[0]]
        mySeq.seq = mySeq.seq.back_transcribe()
        SeqIO.write(mySeq, output_handle, "fasta")
        i += 1

print("Total length {0}".format(totalLength))
print("Not found {0}".format(notFound))

output_handle.close()
output_handle.close()
