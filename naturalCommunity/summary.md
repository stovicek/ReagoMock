# Distant community summary

This is a summary file of the natural community analysis with the **pirs2.0.0** and **reago1.1**
The community was modeled after a community from Ahuva C1 sample from the Ahuva dataset (Zygophyllum and whatnot)

## Versions of used programs

More details about the versions of programs used in this study can be find in the [following fle](programVersions.md).

## Overview of the run

1. [Generating of the community](communityPreparation.md).
  * Setting the abundances of different species (python program)
2. _In silico_ [shotgun sequencing simulation](sequencing/summary.md) (with **Pirs2.0.0**)
3. **Reago1.1** [assembly two step run](assembly/summary.md).
  * Identification of bacterial sequences in the input dataset
  * Gene assembly with **Reago1.1**

## Verification of the results

1. [Taxonomic assignment of the generated contigs with SINA.](verification/summary.md)
