# Mock community file preparation

The mock community was prepared in a following process from a .fasta file with sequences pulled from the SIVA database (community file).

1. Retrieving sequence IDs from the community file with [species_list.py](../../Programs/species_list.py)
* generates a file containing IDs of members of the mock community downloaded from SILVA
2. Manual addition of the desired number of occurences of each seqence in the final file.
* Number of occurences of each taxon are manually added to the list of taxa in the IDfile generated in the process above
* values are separated with a *space*
3. Forming the final community file using the frequencies in the ID list and the original mock community file with [community_file.py](../../Programs/community_file.py)

### 1. Retrieving IDs from community file

```Shell
python3.5 species_list.py --in Mock_community_DNA.fasta --out IDfile.csv
```

### 2. Manual addition of the desired occurrences

The community was designed with a couple of abundant species and a tail of rare species. The structure can be found in [this file](IDfile.csv), consisting of sequence codes and the number of occurrences separated by a space.

### 3. Final community file creation

```Shell
python3.5 community_file.py --list IDfile.csv --community Mock_community.fasta --out Mock_community_DNA_multiple_copies.fasta
```
