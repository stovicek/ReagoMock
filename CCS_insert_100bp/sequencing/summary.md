# Shotgun sequencing

This is a summary of an _in silico_ shotgun sequcing run with the **Pirs2.0.0** program.

## Settings for the run

- Average coverage : 10 fold
- Read length : 100
- Mean insert length : 100
- Insert length standard deviation : 18
- Seed : 777

The community composition was influenced by the frequency of the template e.g. sequence appearing 20 times would be 20 times more represented then sequence appearing only once.  

```Shell
 pirs simulate community/FinalCommunity.fasta --coverage 10 --insert-len-mean 100 --insert-len-sd 18 --random-seed 777 --threads 1 -o sequencing/ >sequencing/simulate_seq.o 2>sequencing/simulate_seq.e
 ```

One thread was used in order to make the results more repeatable.

Details of the run can be retrieved from [this file](simulate_seq.o).

## Transforming .fastq files into .fasta files

In order to perform the assembly, the .fastq files were transformed to .fasta with the following command:

```Shell
sed -n '1~4s/^@/>/p;2~4p' Read1.fq > Read1.fasta
```
