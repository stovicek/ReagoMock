#!/bin/bash

while IFS=, read col1 col2
do
    grep -A1 $col1 /home/stovicek_lab/Bioinformatics/Silva/Silva123__SSURef_tax_oneline.fasta >> ../naturalCommunity/community/Silva_v123_red.fasta
done < ../naturalCommunity/community/idFile.csv
