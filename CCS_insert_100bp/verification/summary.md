# Taxonomic verification for the Close community of Streptomyces with 100bp insert

This is a taxonomic verification of the results from the run.

The full genes as well as the fragments were classified with the SINA aligner at the https://www.arb-silva.de/aligner/.

The results of the classification can be [found here](arb-silva.de_align_resultlist_306197.csv):

## Quality of taxonomic assignment

The quality of the REAGO1.1 run is verified with the SINA aligner. Some statistics was explored with the `qual_plot.py` program that can be found in the folder [**Programs**](../../Programs/qual_plot.py). The command to generate the plots was following:

```Shell
python qual_plot.py -i ../CCS_insert_100bp/verification/arb-silva.de_align_resultlist_306197.csv -o ../CCS_insert_100bp/verification/ -l 60 -u 102  
```

### Assembled contigs

The position (on the vertical axis) of the contigs in relation to an approximate position in _E.coli_ gene.

![Assembled contigs](Assembled_contigs.png)

### Taxonomic assignment similarity score

This is a value determining the quality of the SILVA taxonomic assignment. It has values between 0 to 100.

![Assignment quality](Similarity_plot.png)

### Visual summary of the taxonomic evaluation

This visual was generate in a two step process.

1. Generation of a taxonomic table with a python program `tax_table.py` which can be found [here](../../Programs/tax_table.py).

```Shell
python tax_table.py -i ../CCS_insert_100bp/verification/arb-silva.de_align_resultlist_306197.csv -t ../CCS_insert_100bp/community/community.fasta -o ../CCS_insert_100bp/verification/tax_table.csv -ot ../CCS_insert_100bp/verification/readable.csv
```

2. Plotting of the actual figure in R using a script `tax_plot.R` found [here](../../Programs/tax_plot.R).

```Shell
./tax_plot.R ../CCS_insert_100bp/verification/tax_table.csv ../CCS_insert_100bp/verification/tax_plot.png 800
```
![Taxonomy plot](tax_plot.png)

### **Conclusion**

The results of this run are generally not completely wrong, but there is one outlier. Fortunately this sequence has also a very low template simlarity within Silva v123. This can be in future take as a clue about the low quality sequences.

Surprisingly, no chimeras were recovered with the UCHIME run. This may be due to the community similarities. Results can be [found here](../uchime/summary.md)
