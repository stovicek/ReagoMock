#!/usr/bin/python3.5

import argparse

parser = argparse.ArgumentParser()

parser = argparse.ArgumentParser(description='# This program processing data from otu table of a single sample and taxonomic classification, generating an idFile.')

parser.add_argument('--otu', dest='OTU', help='Input otu table of a single sample.')
parser.add_argument('--tax', dest='TAX', help='Input raw taxonomic output file from SINA aligner.')
parser.add_argument('--out', dest='OUT', help='Output idFile.')
parser.add_argument('--log', dest='LOG', default='NULL' ,help='[OPTIONAL] Log file about the details of the run.')
parser.add_argument('--verb', dest='VERB', default='1' ,help='[OPTIONAL] Verbosity of the on-screen output. 0 - no on-screen output, 1 - returns only percentage and number of discarded sequences, \
2 -  Default: 1')

args = parser.parse_args()

# Writing a log file header.
if args.LOG != "NULL":
    with open(args.LOG, 'w') as fLog:
        import time
        fLog.write("This is a log file for an idFile, generated from a community otu table using an ifFileGenerator.py on the %s. \n\
Three categories of errors are logged: \n \
          \t -[W1] Warning: The taxonomic code of an OTU is already in use. The next suggested hit in the 'neighbourhood' is used instead. Record is NOT discarded.\n \
          \t -[E1] Level 1 error: OTU has not been classified at all. The record is discarded. Final percentage of discarded OTUs is returned on screen after each run. \n \
          \t -[E2] Level 2 error: Too many OTUs of similar taxonomy exist in one 'neighbourhood' and the program run out of replacements.\n \
           The record is discarded.  Final percentage of discarded OTUs is returned on screen after each run \n \
Each of the categories of interest can be retrieved with the grep command (e.g. 'grep 'E1' log.txt'). Number of occurences in each category can be counted as well (e.g. 'grep 'E1' log.txt | wc -l). \
Do not attempt to use '[]' in the grep expression as it will be interpreted as a regular expression and yield unexpected results. \
          \n Following is the list of problematic OTUs:\n \n".format(time.ctime()))

taxDict = {}

totalLength = 0
unclassified = 0

# Building a dictionary of taxonomic information concerning each OTU

with open(args.TAX) as fTax:
    next(fTax) # Skipping the first line
    for line in fTax:
        totalLength += 1
        line = line.split('\t')
        otuName = line[0] # Retrieving the OTU name from the 1st column
        taxCode = line[14].split(' ')[0] # Retrieving the first entry in the 14th column of Silva alignment results
        taxCode = taxCode.split('.')[0] # Trimming the strange random apendages from the code
        if len(line[14]) == 0:  # There is no taxonomic information
            unclassified += 1
            if args.LOG != "NULL":  # Log file message
                with open(args.LOG, 'a') as fLog:
                    fLog.write("\n[E1] {0} is unclassified.".format(otuName))
            continue
        if taxCode not in taxDict.values(): # If the taxonomic code is new, it's added to the dictionary
            taxDict[otuName] = taxCode
        else: # If the taxonomic code is alrady used, the next one in the line will be selected
            taxSplit = line[14].split(' ') # Splitting line by empty space to separate different entries.
            if len(taxSplit) > 1: # If there is more then one entry:
                taxList = taxSplit[1:]  # Getting the second one and on since the first has been used already
            else: # There is not enough taxonomic information
                unclassified += 1
                with open(args.LOG, 'a') as fLog:
                    fLog.write("\n[E2] {0} does not offer unique tax code.".format(otuName))
                continue
            isAssigned = False  # Switch to notify the error message generator that no suitable value was found yet.
            for entry in taxList:
                entry = entry.split('~')[0] # Splitting by '~' to separate entry taxonomic code from a hit quality (tax code comes first)
                entry = entry.split('.')[0] # Trimming the strange appendages from the code
                if entry not in taxDict.values(): # Victory! This taxonomic code is clearly not used yet. It will be used now!
                    taxDict[otuName] = entry # Saving tax code to the dictionary
                    isAssigned = True   # Suitable value was found, all is good.
                    if args.LOG != "NULL":  # Log file message
                        with open(args.LOG, 'a') as fLog:
                            fLog.write("\n[W1] {0}; duplicate code tax code: {1}, replacing with {2}".format(otuName,taxCode,entry))
                    break # Breaking from the loop searching for values, since we have already found one.
                else:
                    pass    # If the tax entry IS already used, keep on searching.
            if isAssigned == False:
                unclassified += 1
                with open(args.LOG, 'a') as fLog:
                    fLog.write("\n[E2] {0} does not offer unique tax code.".format(otuName))

if int(args.VERB) >= 1:
    print("{0:.2f} %% (total of {1}) of sequences were unclassified.".format(100*unclassified/totalLength,unclassified))

linenum = 0
linefound = 0
otuZero = 0
codeLen = 0

with open(args.OTU) as fOtu, open(args.OUT, 'w') as fOutput:
    next(fOtu) # Skipping the first line
    for line in fOtu:
        linenum += 1
        line = line.split(',')
        if line[0] in taxDict:
            linefound += 1
            otuCode = taxDict[line[0]]
            otuCount = line[1]
            if int(otuCount) <= 0:
                otuZero += 1
            if len(otuCode) <= 0:
                codeLen += 1
                print(line)
                print(otuCode)
            if (int(otuCount) > 0 and  len(otuCode) > 0):
                fOutput.write(otuCode + ',' + otuCount)
        else:
            unclassified += 1

if int(args.VERB) >= 2:
    print("The input file contains {0} entries".format(totalLength))

    print("Out of which {0} contained valid OTUs for the further analysis.".format(linenum))
    print("{0} OTUs were successfully matched with the taxonomy dictionary.".format(linefound))

    print("{0} OTUs do not appear in the sample (OTU count is 0).".format(otuZero))
    print("{0} OTUs do not contain taxonomic code.".format(codeLen))
