# Summary of the UCHIME analysis

The analysis was performed with the RDP tranining data set version 15 containint 3000 selected sequences.

The following command was used in order to run the analysis:

```Shell
usearch8 -uchime_ref Projects/ReagoMock/CCS_insert_100bp/assembly/sample_out/fragments.fasta -db Bioinformatics/RDP_transet15/trainset15_092015.fa -uchimeout Projects/ReagoMock/CCS_insert_100bp/uchime/results.uchime -chimeras Projects/ReagoMock/CCS_insert_100bp/uchime/chimeras.uchime -uchimealns Projects/ReagoMock/CCS_insert_100bp/uchime/alignments.uchime -nonchimeras Projects/ReagoMock/CCS_insert_100bp/uchime/nonchimeras.uchime -strand plus
```

## Results

**No chimearas were found** during the analysis. Resulting file is [here](chimeras.uchime).

Results of the analysis can be found in [here](results.uchime).

## Conclusion

It is likely that chimeric sequences are not detected because the dataset originates from one genera _Streptomyces_ (disregarding the wrongly assigned members). Analysis of chimeric sequences works best between distant members. 
